# vcs-sample-main
vcstool の使い方を確認するためのリポジトリです。  
以下2つの外部のgit リポジトリをclone します。  

* vcs-sample-sender : https://gitlab.com/kizaki/vcs-sample-sender
* vcs-sample-receiver : https://gitlab.com/kizaki/vcs-sample-receiver


## vcstool 公式ページ
http://wiki.ros.org/vcstool


## vcstool の使い方
1. インストール
   ```
   $ sudo apt install python-vcstool
   ```
2. vcstool 実行 
   ```
   $ vcs import < vcs.repos
   ```


## アプリケーションの実行
アプリケーションを実行する場合の手順です。

1. ROS (melodic) をインストール  
   以下サイトの手順どおりに実行します。  
   http://wiki.ros.org/melodic/Installation/Ubuntu 

2. vcstool 実行
   ```
   $ vcs import < vcs.repos
   ```

3. ビルド
   ```
   $ catkin_make
   ```

4. 実行  
   **1つ目**のターミナルでroscore を実行
   ```
   $ source /opt/ros/meldic/setup.bash
   $ roscore
   ```
   **2つ目**のターミナルでsender アプリケーションを実行
   ```
   $ source devel/setup.bash
   $ rosrun sender sender
   ```
   **3つ目**のターミナルでreceiver アプリケーションを実行
   ```
   $ source devel/setup.bash
   $ rosrun receiver receiver
   ```
